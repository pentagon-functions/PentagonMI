BeginPackage["PentagonFunctionsM`"];


EvaluatePentagonFunctions::usage = "EvaluatePentagonFunctions[functions, point]
evaluates a list of functions on a generic kinematical point,
given by a list of Mandelstam invariants {s12,s23,s34,s45,s15}.

Result is returned in terms of replacement rules.

Note that the kinematical point is explicitely cast into floating point just before the numerical integration.
This might lead to increased inegrand construction times with exact input.

Options:
--------

WorkingPrecision (default = MachinePrecision) :
set working precision (not precision goal!) which will be used in all evaluations

\"IntegrationPrecisionGoal\" (default = 10) :
precision goal passed to NIntegrate

\"Parallel\" (default = True)
whether to use parallel kernels for numerical integration

";


DeltaS::usage = "A symbolic representation of the Sqrt[Delta]."

s::usage = "A head for Mandelstam invariants s[i,j]";
v::usage = "A head for kinematical variables v[i]";

Begin["`Private`"];

(* F is defined in the C++ interface, so for convinience we "forward" the definition to this context *)
PentagonFunctionsM`F = PentagonFunctions`F;
PentagonFunctionsM`tcr = PentagonFunctions`tcr;
PentagonFunctionsM`tci = PentagonFunctions`tci;

NIntegrateOptions = {Method -> {"DoubleExponential", "SymbolicProcessing" -> False}, MaxRecursion -> 12};

(*SetOptions[NIntegrate, NIntegrateOptions]; *)

$ImportDir = FileNameJoin[{DirectoryName[$InputFileName],"datafiles"}];

Weight1Functions=Get[FileNameJoin[{$ImportDir,"functions_weight1.m"}]];
Weight2Functions=Get[FileNameJoin[{$ImportDir,"functions_weight2.m"}]];

With[
    {
        w3=Get[FileNameJoin[{$ImportDir,"functions_weight3_onefold.m"}]],
        w4=Get[FileNameJoin[{$ImportDir,"functions_weight4_onefold.m"}]]
    },
    Weight34FunctionsDef = Join[w3, w4]//Dispatch;
];

Get[FileNameJoin[{$ImportDir,"constants_numerical.m"}]]

SetAttributes[s,Orderless];

StoV={
    s[1,3]->-v[1]-v[2]+v[4],
    s[1,4]->v[2]-v[4]-v[5],
    s[2,4]->-v[2]-v[3]+v[5],
    s[2,5]->-v[1]+v[3]-v[5],
    s[3,5]->v[1]-v[3]-v[4],
    s[1,2]->v[1],
    s[2,3]->v[2],
    s[3,4]->v[3],
    s[4,5]->v[4],
    s[1,5]->v[5]
};

Delta= v[1]^2 v[2]^2-2 v[1] v[2]^2 v[3]+v[2]^2 v[3]^2+2 v[1] v[2] v[3] v[4]-
        2 v[2] v[3]^2 v[4]+v[3]^2 v[4]^2-2 v[1]^2 v[2] v[5]+2 v[1] v[2] v[3] v[5]+
            2 v[1] v[2] v[4] v[5]+2 v[1] v[3] v[4] v[5]+2 v[2] v[3] v[4] v[5]-2 v[3] v[4]^2 v[5]+v[1]^2 v[5]^2-2 v[1] v[4] v[5]^2+v[4]^2 v[5]^2;

aToV={
    a[26]->v[1] v[2]-v[2] v[3]+v[3] v[4]-v[1] v[5]-v[4] v[5],
    a[27]->-v[1] v[2]+v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5],
    a[28]->-v[1] v[2]-v[2] v[3]+v[3] v[4]+v[1] v[5]-v[4] v[5],
    a[29]->v[1] v[2]-v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5],
    a[30]->-v[1] v[2]+v[2] v[3]-v[3] v[4]+v[1] v[5]-v[4] v[5]
};

WtoVexplicit={
    W[1]->v[1],
    W[2]->v[2],
    W[3]->v[3],
    W[4]->v[4],
    W[5]->v[5],
    W[6]->v[3]+v[4],
    W[7]->v[4]+v[5],
    W[8]->v[1]+v[5],
    W[9]->v[1]+v[2],
    W[10]->v[2]+v[3],
    W[11]->v[1]-v[4],
    W[12]->v[2]-v[5],
    W[13]->-v[1]+v[3],
    W[14]->-v[2]+v[4],
    W[15]->-v[3]+v[5],
    W[16]->v[1]+v[2]-v[4],
    W[17]->v[2]+v[3]-v[5],
    W[18]->-v[1]+v[3]+v[4],
    W[19]->-v[2]+v[4]+v[5],
    W[20]->v[1]-v[3]+v[5],
    W[21]->-v[1]-v[2]+v[3]+v[4],
    W[22]->-v[2]-v[3]+v[4]+v[5],
    W[23]->v[1]-v[3]-v[4]+v[5],
    W[24]->v[1]+v[2]-v[4]-v[5],
    W[25]->-v[1]+v[2]+v[3]-v[5],
    W[26]->(-DeltaS+v[1] v[2]-v[2] v[3]+v[3] v[4]-v[1] v[5]-v[4] v[5])/(DeltaS+v[1] v[2]-v[2] v[3]+v[3] v[4]-v[1] v[5]-v[4] v[5]),
    W[27]->(-DeltaS-v[1] v[2]+v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5])/(DeltaS-v[1] v[2]+v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5]),
    W[28]->(-DeltaS-v[1] v[2]-v[2] v[3]+v[3] v[4]+v[1] v[5]-v[4] v[5])/(DeltaS-v[1] v[2]-v[2] v[3]+v[3] v[4]+v[1] v[5]-v[4] v[5]),
    W[29]->(-DeltaS+v[1] v[2]-v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5])/(DeltaS+v[1] v[2]-v[2] v[3]-v[3] v[4]-v[1] v[5]+v[4] v[5]),
    W[30]->(-DeltaS-v[1] v[2]+v[2] v[3]-v[3] v[4]+v[1] v[5]-v[4] v[5])/(DeltaS-v[1] v[2]+v[2] v[3]-v[3] v[4]+v[1] v[5]-v[4] v[5]),
    W[31]->DeltaS
};

SetAttributes[delta, NHoldAll];
SetAttributes[W, NHoldAll];
Quiet[SetAttributes[F, NHoldAll]];

notFixedSignW={W[7],W[10],W[12],W[21],W[22],W[23]};
analyticContinuationOdd=Thread@Rule[Table[OddLog@W[i],{i,26,30}] , 
Log[Table[W[i],{i,26,30}]]/.Log[W[k_]]/; MemberQ[{26,27,28},k]:> Theta[a[k]]Log[W[k]]+Theta[-a[k]](Log[W[k]]-2I Pi) - I Pi delta[a[k]]/.
Log[W[k_]]/; MemberQ[{29,30},k]:> Theta[-a[k]]Log[W[k]]+Theta[a[k]](Log[W[k]]+2I Pi) + I Pi delta[a[k]]];

basepoint={v[1]->3,v[2]->-1,v[3]->1,v[4]->1,v[5]->-1,DeltaS->I Sqrt[3]};
getpath[endpoint_]:=Thread[Map[v,Range@5] -> (1-t)(Map[v,Range@5]/.basepoint)+t(Map[v,Range@5]/.endpoint)]//Expand;
getpathDelta[endpoint_]:=Append[getpath[endpoint],DeltaS -> Sqrt@Expand[Delta/.getpath[endpoint]]];


IteratedOdd=Map[It,Array[W,5,26]]-> (Map[OddLog,Array[W,5,26]]/.analyticContinuationOdd)-(Map[Log,Array[W,5,26]]/.WtoVexplicit/.basepoint//FullSimplify)//Thread;

Weight1FunctionsAux={
    F[1,3,1]->It@W[26],
    F[1,3,2]->It@W[27],
    F[1,3,3]->It@W[28],
    F[1,3,4]->It@W[29],
    F[1,3,5]->It@W[30],
    F[1,2,10]-> Log[W[31]]
}/.IteratedOdd;


findSubRegion[endpoint_]:=Block[{signature, possibleRegions},
    possibleRegions={
        {-1,-1,-1,-1,-1,1},
        {-1,-1,-1,-1,1,1},
        {-1,-1,-1,1,-1,-1},
        {-1,-1,-1,1,-1,1},
        {-1,-1,-1,1,1,-1},
        {-1,-1,-1,1,1,1},
        {-1,-1,1,-1,-1,-1},
        {-1,-1,1,-1,-1,1},
        {-1,-1,1,-1,1,-1},
        {-1,-1,1,-1,1,1},
        {-1,-1,1,1,-1,-1},
        {-1,-1,1,1,1,-1},
        {-1,1,-1,-1,-1,1},
        {-1,1,-1,1,-1,-1},
        {-1,1,-1,1,-1,1},
        {-1,1,1,-1,-1,-1},
        {-1,1,1,-1,-1,1},
        {-1,1,1,1,-1,-1},
        {1,-1,-1,-1,1,1},
        {1,-1,-1,1,1,-1},
        {1,-1,-1,1,1,1},
        {1,-1,1,-1,1,-1},
        {1,-1,1,-1,1,1},
        {1,-1,1,1,1,-1},
        {1,1,-1,-1,-1,1},
        {1,1,-1,-1,1,1},
        {1,1,-1,1,-1,-1},
        {1,1,-1,1,-1,1},
        {1,1,-1,1,1,-1},
        {1,1,-1,1,1,1},
        {1,1,1,-1,-1,-1},
        {1,1,1,-1,-1,1},
        {1,1,1,-1,1,-1},
        {1,1,1,-1,1,1},
        {1,1,1,1,-1,-1},
        {1,1,1,1,1,-1}
    };
    signature=Map[Sign, notFixedSignW/.WtoVexplicit/.endpoint];
    If[MemberQ[signature,0],
        Print["The point lies on the spurious signularity surface"];
        {Null,signature},
        {First@Flatten@Position[possibleRegions,signature],signature}
    ]
];

findIntervals[Thetalist_List, prec_?NumericQ]:=Block[{nodes,arglist,intervals},
    arglist=Thetalist/.Theta[z_]:>z;
    nodes=Union[Flatten@Map[Flatten@Solve[#== 0&&0<t<1,{t}]&,arglist]][[All,2]];
    nodes=Sort[Join[nodes,{0,1}],#1<#2&];
    intervals=Partition[nodes,2,1];
    {intervals,Map[Thread[Thetalist-> HeavisideTheta@N[arglist/.(t->(Total@#)/2) // Simplify,{prec,prec}]]&,intervals]}
];

Options[EvaluatePentagonFunctions] = {"Parallel" -> True, "CheckZero" -> False, WorkingPrecision -> MachinePrecision, "IntegrationPrecisionGoal" -> 10};

Options[EvaluateWeight12] = {WorkingPrecision -> MachinePrecision};

EvaluateWeight12[{}, ___] := {};

EvaluateWeight12[funListin:{F[1|2,__]..}, point_List, opts: OptionsPattern[]] := Block[
    {
        pointRule=Thread@Rule[Array[v,5],point],
        funList = funListin
    },

    (* only substitue a numerical value of Sqrt[Delta] if it is not exactly zero *)
    If[getKinematicsType[point] == "DeltaSingular", 
        funList = DeleteCases[funListin, F[1,2,10]];
    ];

    result = funList/.Join[Weight1Functions, Weight1FunctionsAux, Weight2Functions];

    result = result/.Join[StoV,aToV,WtoVexplicit];

    result = result/.DeltaS->Sqrt[Delta]/.pointRule/.{delta->KroneckerDelta,Theta-> HeavisideTheta}/.HeavisideTheta[0]->0;

    result = N[Simplify[result], OptionValue[WorkingPrecision]];

    result = Thread[funList -> result];

    If[getKinematicsType[point] == "DeltaSingular" && MemberQ[funListin, F[1,2,10]], 
        AppendTo[result, F[1,2,10] -> Log[DeltaS]];
    ];

    result
];

Clear[getKinematicsType];

getKinematicsType[point:{_?NumericQ..}] := With[
        { 
            MandelstamVar={s[1,2],s[1,3],s[1,4],s[1,5],s[2,3],s[2,4],s[2,5],s[3,4],s[3,5],s[4,5]},
            pointRule = Thread[Array[v,5] -> point]
        },
        If[Not[Position[Chop@Simplify[MandelstamVar/.StoV/.pointRule],0]==={}],
            Print["Singular configuration, one of the madelstam invarints vanishes"];
            Return["Invalid"];
        ];

        If[Chop@Simplify[Delta/.pointRule]>0,
            Print["Not in physical region, Delta > 0 "];
            Return["Invalid"];
        ];

        If[Sign[point] =!= {1, -1, 1, 1, -1},
            Print["Not 12->345 scattering channel!"];
            Return["Invalid"];
        ];

        If[Simplify[Delta/.pointRule]===0,
            Return["DeltaSingular"];
        ];

        "Generic"
];

getKinematicsType::numeric = "Non-numeric kinematics `1`";

getKinematicsType[arg_] := (Message[getKinematicsType::numeric, arg]; Abort[]);

EvaluatePentagonFunctions::unknownf = "Requested unknown function(s)\n`1`!"; 

EvaluatePentagonFunctions[{}, ___] := {};

EvaluatePentagonFunctions[funList:{_F..}, point_List, opts: OptionsPattern[]] := With[
    {
        w12 = Cases[funList,F[1|2,__]],
        w34 = Cases[funList,F[3|4,_]]
    },
    With[
        {others = Complement[funList,w12,w34]},
        If[ others =!= {},
            Message[EvaluatePentagonFunctions::unknownf, others];
            Abort[]
        ]
    ];

    Join[
        EvaluateWeight12[w12, point, FilterRules[{opts}, Options[EvaluateWeight12]]],
        EvaluatePentagonFunctions[w34, point, opts]
    ]
];


EvaluatePentagonFunctions[funList:{F[3|4,_]..}, point_List, opts: OptionsPattern[]] := Block[
    {
        pointRule,path,
        DLogOnPath,aToVOnPath, StoVOnPath,WtoVOnPath,allWeight1FunctionsOnPath,mainWeight2FunctionsOnPath,onPathFunW2Rule, IntDLogOnPath,
        buildIntegrands,integrands,result,NIntegrate$,
        singkinQ = getKinematicsType[point],
        integrationgoal = OptionValue["IntegrationPrecisionGoal"],
        wprec = OptionValue[WorkingPrecision],
        checkZero = OptionValue["CheckZero"]
    },

    singkinQ = Switch[singkinQ, "Generic", False, "DeltaSingular", True, _, Abort[]];

    If[singkinQ,
        Print["Singular kinematics Delta=0"];
    ];

    pointRule=Thread@Rule[Array[v,5],point];

    path=getpathDelta[pointRule];

    DLogOnPath = (DLog[#1] -> With[{w = Simplify[#2/.path]}, If[MemberQ[notFixedSignW,w] && w===0, 0, Simplify[D[Log[w],t]]]])& @@@ WtoVexplicit;

    aToVOnPath=Expand[aToV/.path];
    StoVOnPath=Expand[StoV/.path];
    WtoVOnPath=Simplify[WtoVexplicit/.path];

    allWeight1FunctionsOnPath=Join[Weight1Functions,Weight1FunctionsAux]/.Dispatch@Join[StoVOnPath,WtoVOnPath,aToVOnPath];
    mainWeight2FunctionsOnPath=Weight2Functions/.Dispatch@Join[StoVOnPath,WtoVOnPath];

    onPathFunW2Rule = Join[
        mainWeight2FunctionsOnPath,
        allWeight1FunctionsOnPath/.If[singkinQ,{},delta[_]->0],
        DLogOnPath
    ];

    IntDLogOnPath=Block[{list,ruleOdd,ruleMain,ruleSpur,nonzero,zero, subregion, signature},
        {subregion,signature}=findSubRegion[pointRule];
        list=Table[F[1,3,n]/.allWeight1FunctionsOnPath,{n,1,5}];
        ruleOdd=Thread[Table[IntDLog[W[n]],{n,26,30}]-> Simplify[list/.t->1/.{delta->KroneckerDelta,Theta-> HeavisideTheta}/.HeavisideTheta[0]->0]-(list/.delta[_]:>0)];
        list=Weight1Functions[[All,1]]/.onPathFunW2Rule;
        ruleMain=Thread[Map[IntDLog,{W[1],W[2],W[3],W[4],W[5],W[16],W[17],W[18],W[19],W[20],W[6],W[8],W[9],W[11],W[13],W[14],W[15],W[24],W[25]}]->(Simplify[list/.t->1])-list ];
        zero=Flatten@Position[signature,0];
        nonzero=Complement[Range@6,zero];
        list=Log[(notFixedSignW signature)[[nonzero]]/.WtoVexplicit/.path//Expand//Simplify];
        ruleSpur=Thread[Map[IntDLog,notFixedSignW[[nonzero]]]-> Simplify[list/.t->1]-list];
        ruleSpur=Join[ruleSpur,Thread[Map[IntDLog,notFixedSignW[[zero]]]-> Table[0,zero//Length]]];
        Join[ruleMain,ruleSpur,ruleOdd]
    ];

    If[!singkinQ,
        With[{list=(Log[DeltaS]/.path)},
            AppendTo[IntDLogOnPath,IntDLog[W@31]->(Simplify[list/.t->1])-list];
        ]
    ];

    onPathFunW2Rule=Dispatch@Join[IntDLogOnPath,Normal@onPathFunW2Rule];
    
    If[singkinQ,
        buildIntegrands[fun_] := buildIntegrandsDeltaSingular[fun, onPathFunW2Rule, WtoVOnPath, wprec, NIntegrate$];
        ,
        buildIntegrands[fun_] := buildIntegrandsGeneric[fun, onPathFunW2Rule, wprec, checkZero, NIntegrate$];
    ];

    PrintTemporary["Constructing integrands..."];
    If[TrueQ[OptionValue["Parallel"]],
        LaunchKernels[];
        DistributeDefinitions[buildIntegrands, tci, tcr, PentagonFunctions`tci, PentagonFunctions`tcr];
        integrands=ParallelMap[buildIntegrands,funList/.Weight34FunctionsDef, DistributedContexts -> None];
        ,
        integrands=Map[buildIntegrands,funList/.Weight34FunctionsDef];
    ];

    If[TrueQ[OptionValue["Parallel"]],
        PrintTemporary["Integrating ", Length[funList], " functions..."];
        DistributeDefinitions[integrationgoal, NIntegrateOptions];
        result = Block[{intpos = Position[integrands, _NIntegrate$], integrated},
            integrated = ParallelMap[
                NIntegrate[First@#,Last@#, Evaluate[{PrecisionGoal->integrationgoal, AccuracyGoal->integrationgoal, WorkingPrecision->integrationgoal+4,NIntegrateOptions}//Flatten]]&,
                Extract[integrands,intpos],
                DistributedContexts -> None
            ];
            ReplacePart[integrands, Thread[intpos -> integrated]]
        ];
        CloseKernels[];
        ,
        Monitor[
            result = Table[
                message="Integrating : "<>ToString[n]<>" out of "<>ToString[Length@integrands];

                integrands[[n]]/.NIntegrate$[f_,l_] :> 
                    NIntegrate[f,l, Evaluate[{PrecisionGoal->integrationgoal, AccuracyGoal->integrationgoal, WorkingPrecision->integrationgoal+4,NIntegrateOptions}//Flatten]],

                {n,1,integrands//Length}
            ] ,
            message
        ]
    ];

    Thread[funList-> Map[Total,result]]
];

(* TODO: this constructoin is rather arbitary, can be improved *)
zeroIntegrandQ[z_,{t_,a_,b_}, wprec_] := Block[{
        rlist=Table[{t-> RandomReal[{a,b}, WorkingPrecision-> wprec]},3]
    }, 
    Max[Abs[z/.rlist]] < 10^(3-N[wprec,wprec])
];


buildIntegrandsGeneric[fun_, onPathFunW2Rule_, wprec_, checkZero_,nIntegrate$_] := Block[{var,integrand,intervals,tab},
    var=Cases[fun,_DLog,Infinity];
    integrand=(var/.onPathFunW2Rule).Expand[Normal@Last@CoefficientArrays[fun,var]/.onPathFunW2Rule];
    intervals=findIntervals[Union@Cases[integrand,Theta[_],Infinity], wprec];
    tab=Table[
        nIntegrate$[integrand/.Dispatch[Last[intervals][[n]]],Prepend[First[intervals][[n]],t]],
        {n,1,Length@Last[intervals]}
    ];

    (* detect heuristically if the integrand is zero *)
    If[checkZero,
        tab = tab/.nIntegrate$[args__]/;zeroIntegrandQ[args, wprec]:>Nothing;
    ];

    N[tab,{wprec, wprec}]

];

buildIntegrandsDeltaSingular[fun_, onPathFunW2Rule_, WtoVOnPath_, wprec_, nIntegrate$_]:=Block[
    {
        var,integrand,intervals, tab, w31onpath, logDeltaSexp, del, logdel, funi, funW31coeffs,
        tab0, tab1, tab2, tab3
    },

    var=Cases[fun,_DLog,Infinity];
    w31onpath=1/2 Log[-(W[31]/.WtoVOnPath)^2//Simplify]+I Pi/2;
    logDeltaSexp=Expand@Normal@Series[Expand[w31onpath/.t->1-del],{del,0,0},Assumptions->del>0];

    funW31coeffs = CoefficientRules[fun , {DLog[W[31]], IntDLog[W[31]]}]//Association;

    funi = funW31coeffs[{0,0}] // Replace[_Missing->0];
    integrand=(var/.onPathFunW2Rule).Expand[Normal@Last@CoefficientArrays[funi,var]/.onPathFunW2Rule/.delta[_]:>0];
    intervals=findIntervals[Union@Cases[integrand,Theta[_],Infinity], wprec];
    tab0=Table[
        nIntegrate$[integrand/.Dispatch[Last[intervals][[n]]],Prepend[First[intervals][[n]],t]],
        {n,1,Length@Last[intervals]}
    ];

    funi = funW31coeffs[{1,0}]/.onPathFunW2Rule // Replace[_Missing->0];
    integrand=funi (DLog[W@31]/.onPathFunW2Rule);
    intervals=findIntervals[Union@Cases[integrand,Theta[_],Infinity], wprec];
    tab1=Table[
        nIntegrate$[integrand/.delta[_]:>0/.Dispatch[Last[intervals][[n]]],Prepend[First[intervals][[n]],t]],
        {n,1,Length@Last[intervals]-1}
    ];
    tab1 = Join[
        tab1,
        {nIntegrate$[(DLog[W@31]/.onPathFunW2Rule)((funi/.delta[_]:>0/.Dispatch[Last[intervals]//Last])-
            (funi/.Dispatch[Last[intervals]//Last]/.t->1/.delta->KroneckerDelta)),Prepend[First[intervals]//Last,t]]}
    ];
    term1=(logDeltaSexp-(w31onpath/.t->First@Last@First[intervals] ))(funi/.Dispatch[Last[intervals]//Last]/.t->1/.delta->KroneckerDelta);


    funi = funW31coeffs[{0,1}]/.onPathFunW2Rule // Replace[_Missing->0];
    intervals=findIntervals[Union@Cases[funi,Theta[_],Infinity], wprec];
    tab2 = Table[
        {
            Log[DeltaS]nIntegrate$[funi/.delta[_]:>0/.Dispatch[Last[intervals][[n]]],Prepend[First[intervals][[n]],t]],
            -nIntegrate$[(funi/.delta[_]:>0/.Dispatch[Last[intervals][[n]]])w31onpath,Prepend[First[intervals][[n]],t]]
        },
        {n,1,Length@Last[intervals]}
    ] // Flatten;

    funi = funW31coeffs[{1,1}]/.onPathFunW2Rule // Replace[_Missing->0];
    integrand=funi (DLog[W@31]/.onPathFunW2Rule);
    intervals=findIntervals[Union@Cases[integrand,Theta[_],Infinity], wprec];
    tab3=Table[
        {
            Log[DeltaS] nIntegrate$[integrand/.delta[_]:>0/.Dispatch[Last[intervals][[n]]],Prepend[First[intervals][[n]],t]],
            -nIntegrate$[(integrand/.delta[_]:>0/.Dispatch[Last[intervals][[n]]])w31onpath,Prepend[First[intervals][[n]],t]]},
        {n,1,Length@Last[intervals]-1}
    ] // Flatten;
    tab3=Join[
        tab3,
        {
            Log[DeltaS] nIntegrate$[
                (DLog[W@31]/.onPathFunW2Rule)((funi/.delta[_]:>0/.Dispatch[Last[intervals]//Last])-(funi/.Dispatch[Last[intervals]//Last]/.t->1/.delta->KroneckerDelta)),
                Prepend[First[intervals]//Last,t]
            ],
            -nIntegrate$[
                (DLog[W@31]/.onPathFunW2Rule)((funi/.delta[_]:>0/.Dispatch[Last[intervals]//Last])-(funi/.Dispatch[Last[intervals]//Last]/.t->1/.delta->KroneckerDelta))w31onpath,
                Prepend[First[intervals]//Last,t]
            ]
        }
    ];
    term2=(funi/.Dispatch[Last[intervals]//Last]/.t->1/.delta->KroneckerDelta)/2(logDeltaSexp-(w31onpath/.t->First@Last@First[intervals] ))^2;

    (* detect heuristically if the integrand is zero *)
    tab = Join[tab0,tab1,tab2,tab3]/.nIntegrate$[args__]/;zeroIntegrandQ[args,wprec] :> 0;

    term1 = term1+term2/.Flatten[Solve[(logDeltaSexp/.Log[del]->logdel)==Log[DeltaS],logdel]/.logdel->Log[del]]//Expand;

    N[Prepend[tab, term1], {wprec,wprec}]
];


End[];

EndPackage[];
