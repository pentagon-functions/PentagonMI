BeginPackage["PentagonMI`"];

Unprotect["PentagonMI`*"];
Unprotect["PentagonMI`Private`*"];

EvaluateMI::usage = "EvaluateMI[MIs, point]
evaluates a list of master integrals MIs (each in the format M[perm, n]) with 
    M -- integral topology (P, PB, HB, DP)
    perm -- index of permutation,
    n -- index of the master integral,
on a kinematical point, given by 5 Mandelstam invariants {s12,s23,s34,s45,s15}.
Sign[eps5] is +1 here (see option \"ParityConjugation\").

Options:
--------

\"MaximalWeight\" (default = 4)
the maximal weight to which the integrals will be evaluated

\"ParityConjugation\" (default = False)
whether to parity conjugate odd integrals

WorkingPrecision (default = MachinePrecision) :
set working precision (not precision goal!) which will be used in evaluations of pentagon functions

\"UseCppLib\" (default = True if available) :
Whether to use the C++ library for evaluation of pentagon functions.

";


GetMIinF::usage = "GetMIinF[TT[perm,n]]
returns an expression in terms of pentagon functions for the master integral *n* from topology *TT* in permutation *perm*.
"

P;PB;HB;DP;

Begin["`Private`"];

If[FindFile["PentagonMI`PentagonFunctionsM`"] === $Failed,
    Print["ERROR: PentagonFunctionsM package not found!\nMake sure that PentagonMI is a subdirectory of one of the directories in System`$Path"];
    Abort[];
];


Get["PentagonMI`PentagonFunctionsM`"];

$CppAvailabale = False;

With[
    { cppinterface = FindFile["PentagonFunctions`"] },
    If[cppinterface === $Failed,
        Print["PentagonFunctions++ was not found! C++ interface disabled."];
        ,
        Needs["PentagonFunctions`"];
        $CppAvailabale = True;
    ];
];

G;

$ImportDir = FileNameJoin[{DirectoryName[$InputFileName],"datafiles"}];

$topologymap = <| P -> 1, PB -> 2, HB -> 3, DP -> 4 |>;

pwd = Directory[];

$cacheFile = FileNameJoin[{$ImportDir,"MI_data_cached.mx"}];

Get[FileNameJoin[{$ImportDir,"constants_numerical.m"}]];

If[FileExistsQ[$cacheFile],

    Get[$cacheFile];
    PrintTemporary["Loaded data files from binary cache."];
    ,

    PrintTemporary["Loading data files..."];
    $allintegrals = Get[FileNameJoin[{$ImportDir, "MI_in_G.m"}]];

    ImportGs[ii_]:=Get[FileNameJoin[{$ImportDir,"GtoF_weight"<>ToString[ii]<>".m"}]];

    ClearAll[gs];
    gs[0]= Get[FileNameJoin[{$ImportDir,"initial_values_weight0.m"}]];
    Table[gs[i] = Dispatch[ImportGs[i]];,{i,4}];

    PrintTemporary["Creating binary cache..."];

    DumpSave[$cacheFile,{$allintegrals, gs}];

];

$allpermutations = Get[FileNameJoin[{$ImportDir, "permutations.m"}]];
$paritygradinginfo = Get[FileNameJoin[{$ImportDir, "parity-grading.m"}]];

parityOddIntegralQ[(h:Alternatives@@(Keys[$topologymap]))[pn_, min_]] := MemberQ[$paritygradinginfo["MI"][[$topologymap[h]]], min];

SetDirectory[pwd];
Clear[pwd];

Block[{ 
        PrecisionOptions = {WorkingPrecision -> MachinePrecision},
        cppopt = {"UseCppLib" -> $CppAvailabale}
    },
    Options[EvaluateMI] = Join[PrecisionOptions,cppopt, {"MaximalWeight" -> 4, "ParityConjugation" -> False}];
    Options[evalGs] = Join[PrecisionOptions,cppopt, {"MaximalWeight" -> 4}];
    Options[evalFsCPP] = PrecisionOptions;
];


evalFsCPP::cpplib = "Failed to start an evaluator process! Is c++ library installed?";
evalFsCPP::precnimpl = "Evaluation with precision `1` is not implemented!";

evalFsCPP$choosePrecision[MachinePrecision] := "double";
evalFsCPP$choosePrecision[prec_?Positive] := "double" /; prec < N[MachinePrecision];
evalFsCPP$choosePrecision[prec_] := "quadruple" /; prec>=N[MachinePrecision] && prec <= 32;
evalFsCPP$choosePrecision[prec_] := "octuple" /; digits>32 && digits <= 64;
evalFsCPP$choosePrecision[prec_] := Null;

evalFsCPP[flist:{_PentagonFunctions`F..}, point:{_?NumericQ..}, opts : OptionsPattern[]] := Block[
    {
        fvals, evaluator,
        precision = evalFsCPP$choosePrecision[OptionValue[WorkingPrecision]]
    },

    If[precision === Null,
        Message[evalFsCPP::precnimpl, OptionValue[WorkingPrecision]];
        Abort[];
    ];

    PrintTemporary["C++ interface with type \"",precision,"\" invoked"];

    evaluator = PentagonFunctions`StartEvaluatorProcess[neededFs,"Precision"->precision];

    If[!TrueQ[ProcessStatus[evaluator[[1]],"Running"]],
        Message[evalFsCPP::cpplib]; 
        Abort[];
    ];

    fvals = PentagonFunctions`EvaluateFunctions[evaluator, point];

    KillProcess[evaluator[[1]]];

    fvals
];

evalGs[glist:{_G..}, point:{_?NumericQ..}, opts : OptionsPattern[]] := Block[
    {
        GsinFs, neededFs
    },

    If[OptionValue["MaximalWeight"] > 4 || OptionValue["MaximalWeight"] < 1, Abort[]];

    GsinFs = neededGs /. Table[gs[i],{i,1,OptionValue["MaximalWeight"]}] // Transpose;

    neededFs = Cases[GsinFs, _PentagonFunctions`F, Infinity] // DeleteDuplicates;

    If[TrueQ[OptionValue["UseCppLib"]] && !$CppAvailabale,
        Message[EvaluateMI::nocpplib];
    ];

    If[$CppAvailabale && TrueQ[OptionValue["UseCppLib"]],
        neededFs = evalFsCPP[neededFs, point, FilterRules[{opts}, Options[evalFsCPP]]];
        ,
        neededFs = PentagonFunctionsM`EvaluatePentagonFunctions[neededFs,point,FilterRules[{opts}, Options[PentagonFunctionsM`EvaluateFunctionsGeneric]]];
    ];

    GsinFs /. Dispatch[neededFs]
];

permute::usage = "Use permutaiton perm to permute s[i,j] and DeltaS in expression expr";

permute[perm_List][expr_] := With[
    {
        signature = Signature[perm],
        rules = Thread[Range[Length[perm]] -> perm]
    },
    expr /. {s[i_, j_] :> (s[i, j] /. rules), DeltaS -> signature DeltaS}
];

getPermutationTo12::usage = "Given a kinematical point X1 and a sign of eps5 in any scattering region, 
finds a permutation S that maps it to a point X in the 12-channel.
Returns {S, X}.
";

getPermutationTo12[sijin_List, dsign : (1 | -1)] := With[
    {
        p = Thread[Array[PentagonFunctionsM`v, 5] -> sijin],
        P0 = {s[1, 2], s[2, 3], s[3, 4], s[4, 5], s[1, 5], dsign * DeltaS}
    },
    Block[{res},
        res = SelectFirst[$allpermutations, (Sign[(permute[#][P0] /. PentagonFunctionsM`Private`StoV /. DeltaS -> 1 /.  p )] == {1, -1, 1, 1, -1, 1}) & ];
        {res, permute[res][P0 // Most] /. PentagonFunctionsM`Private`StoV /. p} 
    ]
];

EvaluateMI::nocpplib = "Requested evaluation of pentagon functions through C++ ingerface, but the C++ library was not found!"

EvaluateMI[mis:{_[_Integer, _Integer]..}, pointin:{_?NumericQ..}, opts:OptionsPattern[]] := Block[
    {
        MIinG, neededGs, w0, permTo12Inv,
        changePermIndex,
        point = N[pointin, OptionValue[WorkingPrecision]]
    },

    permTo12Inv = getPermutationTo12[point, 1];

    (* choose a new permutation index, adjusting for map to 12-channel *)
    (* note that PermutationProduct in Mathematica is in left-to-right order *)
    changePermIndex[pn_] := changePermIndex[pn] = FirstPosition[$allpermutations, PermutationProduct[$allpermutations[[pn]], InversePermutation[permTo12Inv//First]]]//First;

    Print["Scattering in ", permTo12Inv[[1,1;;2]]," channel."];

    (* get initial values *)
    w0 = gs[0][[$topologymap[Head[#]], #[[2]]]]& /@ mis;

    MIinG = {#[[1]]//changePermIndex, $topologymap[#[[0]]],#[[2]]} & /@ mis;
    MIinG = Extract[$allintegrals, MIinG];

    neededGs = Cases[MIinG, _G, Infinity] // DeleteDuplicates;

    neededGs = Thread[neededGs -> evalGs[neededGs, permTo12Inv//Last, FilterRules[{opts}, Options[evalGs]]]];

    MIinG = MIinG /. neededGs /. c:(_tci|_tcr) :> N[c, OptionValue[WorkingPrecision]];

    Table[
        If[OptionValue["ParityConjugation"] && parityOddIntegralQ[mis[[i]]], -1, 1] *
            Prepend[MIinG[[i]],w0[[i]]],
        {i,Length[w0]}
    ]
];

Options[GetMIinF] = {
    "ParityConjugation" -> False,
    "MaximalWeight" -> 4
};

GetMIinF$results;

GetMIinF[top_[perm_Integer,n_Integer], opts:OptionsPattern[]] := GetMIinF$results[top[perm,n], opts] = Block[
    {
        w0, result, vars, lci, lcr
    },

    (* get initial values *)
    w0 = gs[0][[$topologymap[top], n]];

    result = $allintegrals[[perm, $topologymap[top], n]];

    If[OptionValue["MaximalWeight"] > 4 || OptionValue["MaximalWeight"] < 1, Abort[]];

    result = result /. G[ii_]:> Table[G[ii]/.gs[i],{i,1,OptionValue["MaximalWeight"]}] /. {tcr->lcr, tci->lci};

    vars =Variables[result];

    result = FromCoefficientRules[CoefficientRules[#, vars], vars] & /@ result;

    If[OptionValue["ParityConjugation"] && parityOddIntegralQ[top[perm,n]], -1, 1] * 
        Prepend[result,w0] /. {lcr->tcr, lci->tci}
];


End[];

Protect["PentagonMI`*"];
Protect["PentagonMI`Private`*"];

Unprotect[PentagonMI`Private`GetMIinF$results];


EndPackage[];
