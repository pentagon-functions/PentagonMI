PentagonMI
===============

*Dmitry Chicherin*, *Vasily Sotnikov*


`PentagonMI` is a *Mathematica* package for numerical evaluation of a complete set of planar and non-planar
one-loop and two-loop five-point massless master integrals (MI).
The MI are of uniform transcendental weight and are expressed in a basis of
transcendental functions -- `pentagon functions`.
MI can be evaluated numerically in any physical scattering channel.

For details of classification of pentagon functions, see the [publication].

If you use this package in your research, please cite  [publication].


Structure
-------------

Our definitions of MI and pentagon functions can be found in the folder **datafiles/**.
The files are in *Mathematica* format. However, with the exception of **datafiles/constants_numerical.m**,
they are made to be self-consistent and easily understandable as plain text, such that
they can be used outside of this package.
See the file **datafiles/README.md** for details.


We define 4 integral topologies for the MI integrals, and provide analytic expressions for all `5!`
permutations of each of them. We use the following abbreviations of integral topologies:

```
P   = "pentagon"
PB  = "planar pentagon-box"
HB  = "hexagon-box"
DP  = "double pentagon"
```

Everything related to the assembly of MI from pentagon functions in any scattering channel is implemented in **PentagonMI.m**.
Loading data files might take some time on the first usage of the package.
But after that a binary cache file is created, which speeds up the loading.
The interface is provided by the single function `EvaluateMI`.
The integral families are identified by the symbols in the table above.

The core of evaluation of MIs is the numerical evaluation of pentagon functions.
This is implemented in a separate (standalone) package in **PentagonFunctionsM.m**.
The interface for evaluation of pentagon functions is the function `EvaluatePentagonFunctions`.

The evaluation of pentagon functions can be computationally intensive.
For this reason we implemented a dedicated C++ library [PentagonFunctions++].
Its *Mathematica* interface is a "drop-in" replacement for the package `PentagonFunctionsM`,
which is detected automatically (and preferred) if the C++ library is installed.



Tests and examples
-------------------

The implementation is validated by tests, which can be found in the folder **test/**.

The tests are also well suited as examples of the usage of the package.

An example of numerical evaluation of MIs can be found in 
**test/all_master_integrals.m**.

An example of numerical evaluation of pentagon functions can be found in 
**test/compare_functions.m**.

An example of numerical evaluation of pentagon functions in a singular limit can be found in 
**test/functions_delta_singular.m**.


Installation
---------------

`PentagonMI` has a layout of a standard *Mathematica* package.
To use it, check out the complete source tree of this repository 
to any location listed in *Mathematica*'s search path (variable `$Path`).
Alternatively, add the location of the parent of this directory to `$Path`.

Then, the package can be loaded by

```
Needs["PentagonMI`"];
```

Consult the documentation of `Needs` to learn about *Mathematica* packages.

To take advantage of the fast evaluation of pentagon functions, we recommend to use the C++ library [PentagonFunctions++].
[PentagonFunctions++] provides a *Mathematica* interface through a package *PentagonFunctions*.
Make sure that this package is visible to *Mathematica* (through `$Path`).


------------

[publication]: https://arxiv.org/abs/2009.07803
[PentagonFunctions++]: https://gitlab.com/pentagon-functions/PentagonFunctions-cpp
