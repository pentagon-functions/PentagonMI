{F[2,1,1] -> PolyLog[2, 1 - s[3, 4]/s[1, 2]], 
 F[2,1,2] -> PolyLog[2, 1 - s[4, 5]/s[1, 2]], 
 F[2,1,3] -> -(Log[-(s[4, 5]/s[2, 3])]*Log[1 - s[4, 5]/s[2, 3]]) - 
   PolyLog[2, s[4, 5]/s[2, 3]], F[2,1,4] -> 
  PolyLog[2, 1 - s[1, 5]/s[2, 3]], F[2,1,5] -> 
  -(Log[1 - s[1, 5]/s[3, 4]]*Log[-(s[1, 5]/s[3, 4])]) - 
   PolyLog[2, s[1, 5]/s[3, 4]], F[2,1,6] -> 
  -(Log[1 - s[1, 3]/s[4, 5]]*Log[-(s[1, 3]/s[4, 5])]) - 
   PolyLog[2, s[1, 3]/s[4, 5]], F[2,1,7] -> 
  PolyLog[2, 1 - s[2, 4]/s[1, 5]], F[2,1,8] -> 
  PolyLog[2, 1 - s[2, 4]/s[1, 3]], F[2,1,9] -> 
  PolyLog[2, 1 - s[3, 5]/s[1, 2]], F[2,1,10] -> 
  -(Log[-(s[3, 5]/s[2, 4])]*Log[1 - s[3, 5]/s[2, 4]]) - 
   PolyLog[2, s[3, 5]/s[2, 4]], F[2,1,11] -> 
  PolyLog[2, 1 - s[1, 4]/s[2, 3]], F[2,1,12] -> 
  -(Log[1 - s[1, 4]/s[3, 5]]*Log[-(s[1, 4]/s[3, 5])]) - 
   PolyLog[2, s[1, 4]/s[3, 5]], F[2,1,13] -> 
  -(Log[1 - s[2, 5]/s[3, 4]]*Log[-(s[2, 5]/s[3, 4])]) - 
   PolyLog[2, s[2, 5]/s[3, 4]], F[2,1,14] -> 
  PolyLog[2, 1 - s[2, 5]/s[1, 3]], F[2,1,15] -> 
  PolyLog[2, 1 - s[2, 5]/s[1, 4]], F[2,2,1] -> 
  PolyLog[2, W[27]^(-1)] - PolyLog[2, W[27]] + PolyLog[2, W[28]^(-1)] - 
   PolyLog[2, 1/(W[27]*W[28])] - PolyLog[2, W[28]] + PolyLog[2, W[27]*W[28]], 
 F[2,2,2] -> PolyLog[2, W[28]^(-1)] - PolyLog[2, W[28]] + 
   PolyLog[2, W[29]^(-1)] - PolyLog[2, 1/(W[28]*W[29])] - PolyLog[2, W[29]] + 
   PolyLog[2, W[28]*W[29]], F[2,2,3] -> PolyLog[2, 1/(W[27]*W[28])] - 
   PolyLog[2, W[27]*W[28]] + PolyLog[2, (W[26]*W[27])/W[29]] - 
   PolyLog[2, W[26]/(W[28]*W[29])] - PolyLog[2, W[29]/(W[26]*W[27])] + 
   PolyLog[2, (W[28]*W[29])/W[26]], 
 F[2,2,4] -> PolyLog[2, W[26]^(-1)] - PolyLog[2, W[26]] + 
   PolyLog[2, W[30]^(-1)] - PolyLog[2, 1/(W[26]*W[30])] - PolyLog[2, W[30]] + 
   PolyLog[2, W[26]*W[30]], F[2,2,5] -> -PolyLog[2, 1/(W[26]*W[27])] + 
   PolyLog[2, W[26]*W[27]] + PolyLog[2, W[28]/(W[26]*W[30])] - 
   PolyLog[2, (W[27]*W[28])/W[30]] - PolyLog[2, (W[26]*W[30])/W[28]] + 
   PolyLog[2, W[30]/(W[27]*W[28])], 
 F[2,2,6] -> PolyLog[2, W[29]^(-1)] - PolyLog[2, W[29]] + 
   PolyLog[2, W[30]^(-1)] - PolyLog[2, 1/(W[29]*W[30])] - PolyLog[2, W[30]] + 
   PolyLog[2, W[29]*W[30]], F[2,2,7] -> PolyLog[2, W[26]/(W[28]*W[29])] - 
   PolyLog[2, (W[28]*W[29])/W[26]] + PolyLog[2, W[28]/(W[26]*W[30])] - 
   PolyLog[2, 1/(W[29]*W[30])] - PolyLog[2, (W[26]*W[30])/W[28]] + 
   PolyLog[2, W[29]*W[30]], F[2,2,8] -> PolyLog[2, (W[26]*W[27])/W[29]] - 
   PolyLog[2, W[29]/(W[26]*W[27])] + PolyLog[2, 1/(W[26]*W[30])] - 
   PolyLog[2, W[27]/(W[29]*W[30])] - PolyLog[2, W[26]*W[30]] + 
   PolyLog[2, (W[29]*W[30])/W[27]], 
 F[2,2,9] -> PolyLog[2, 1/(W[28]*W[29])] - PolyLog[2, W[28]*W[29]] + 
   PolyLog[2, (W[27]*W[28])/W[30]] - PolyLog[2, W[27]/(W[29]*W[30])] - 
   PolyLog[2, W[30]/(W[27]*W[28])] + PolyLog[2, (W[29]*W[30])/W[27]]}
