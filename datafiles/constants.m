{tci[1, 1] -> I*Pi, tci[1, 2] -> I*Pi, 
 tci[2, 1] -> I*Im[PolyLog[2, 1/2 + (I/2)*Sqrt[3]]], 
 tci[3, 1] -> I*Im[PolyLog[3, I/Sqrt[3]]], 
 tci[3, 2] -> I*Im[PolyLog[3, 1 + I*Sqrt[3]]], 
 tci[4, 1] -> I*Im[PolyLog[4, 1/2 + (I/2)*Sqrt[3]]], 
 tci[4, 2] -> I*Im[PolyLog[4, I/Sqrt[3]]], 
 tci[4, 3] -> I*(5*Im[Li[{2, 2}, {1/2, 1/2 + (I/2)*Sqrt[3]}]] + 
    6*Im[PolyLog[4, 1 - I/Sqrt[3]]] + 6*Im[PolyLog[4, 1 + I*Sqrt[3]]]), 
 tcr[1, 1] -> Log[3], tcr[1, 2] -> Log[2], tcr[2, 1] -> PolyLog[2, 2/3], 
 tcr[3, 1] -> PolyLog[3, 2/3], tcr[3, 2] -> PolyLog[3, 1/4], 
 tcr[3, 3] -> Zeta[3], tcr[4, 1] -> PolyLog[4, 1/3], 
 tcr[4, 2] -> PolyLog[4, 1/2], tcr[4, 3] -> PolyLog[4, 1/4], 
 tcr[4, 4] -> PolyLog[4, 2/3], tcr[4, 5] -> PolyLog[4, 3/4]}
