This folder contains the data files with our definitions of pentagon functions and master integrals in the [publication].
Some details about the contents are given below.  

UT-MI
---------------

The folder `UT-MI` contains the definitions of UT master integrals in terms of the 
integral families G[tau, {..}], introduced in section 3,
for each of the four integral topologies `tau`.
Coresponds to the linear transformation in eq. (4.5).
The following abbreviations are used:

```
P   = "pentagon"
PB  = "planar pentagon-box"
HB  = "hexagon-box"
DP  = "double pentagon"
```

In these files we use 
`eps = (4 - d)/2`, the dimensional-regularization parameter,
`eps5`, the parity-odd invariant introduced in eq. (2.2)


permutations.m
-----------------

This file contains a list of 120 permutations, which are used for indexing
the permutations of master integrals in **MI_in_G.m**.
The first permutation in the list is the "standard" permutation, for which
the integral topologies are defined in *UT-MI*.



parity-grading.m
-----------------

This file contains a map (Mathematica's *Association*), which
give lists of the following parity-odd objects:

* `"DLogW"` : indices of parity-odd (dlog of) letters of the alphabet
* `"MI"`    : indices of parity-odd master integrals for integral topologies `{P,PB,HB,DP}`
* `"F"`     : indices of parity-odd pentagon functions, graded by weight starting from 1
* `"tci"`   : indices of parity-odd imaginary transcendental constants, graded by weight starting from 1

The objects of each type with the indices not belonging to these lists are parity-even.
All real transcendental constants `tcr[w,i]` are parity-even.



MI_in_G.m
--------------

This file contains all UT master integrals written in terms of 
1917 different combinations of pentagon functions, which we denote `G`.
This organization of master integrals stems from topological relations between different permutations,
but details of this are not important. Here we use this merely to compress the data and save disk space.

The file is a nested list of depth 3. The nesting levels, from outer to inner mean
1. integral topology in order `{P,PB,HB,DP}`
2. index of permutation (as listed in **permutations.m**)
3. index of master integrals (as listed in **UT-MI**)



GtoF_weight<i>.m
------------------

These files contain the definitions of weight `i` components of 1917 `G` integrals
in terms of the pentagon functions `F` and transcendental constants `tci` and `tcr`.
The latter denote imaginary and real constants respectively.



initial_values_weight0.m
--------------------------

In this file the rational (weight 0) constants for the four integral topologies
```
{"pentagon", "planar pentagon-box", "hexagon-box", "double pentagon"}
```
are listed. They are invariant under permutations.



constants.m
-----------------

This file constains the definitions of all algebraically independent transcendental constants `tcr` and `tci`
in terms of multiple polylogarithms, that are required to express all UT master integrals.
Note that `tci[1,2]` is parity-even `I*Pi`, which is numerically equal to parity-odd `tci[1,1]`.


constants_numerical.m
----------------------

This file containts the numerical values with 300 digits precision of the transcendental constants.


functions_weight1,2.m
-------------------------

These two files contain the definitions of weight 1 and weight 2 pentagon functions.


functions_weight3,4_onefold.m
----------------------------------

These two files contain the definitions of the integrands of
the one-fold integral representation of weight 3 and weight 4 pentagon functions.
Here we use the notation

```
DLog[W[i]]      -- dlog integration kernel of letter W[i]
IntDLog[W[i]]   -- integral of dlog kernel of letter W[i] 
```
see section 5.6.2 of [publication] for details.

-------------------------

[publication]: https://arxiv.org/abs/2009.07803
