#!/bin/sh

errors=0
failed=0

for t in *.m
do
    ((total=total+1))
    echo "******************************************"
    echo "Running test $t";
    echo "******************************************"
    if math -script "./$t"; then
        echo "" 
        echo ""
        echo "test $t passed"
    else
        echo ""
        echo ""
        echo "test $t FAILED"
        ((failed=failed+1))
    fi
    echo "******************************************"
    echo "" 
    echo ""
done

if [ $errors -eq 0 ]; then
    echo "$total tests passed"
else
    echo "$failed/$total tests failed!"
fi
