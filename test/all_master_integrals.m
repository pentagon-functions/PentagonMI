Needs["PentagonMI`", "../PentagonMI.m"];

X1point = {4, -(113/47), 281/149, 349/257, -(863/541)};

(* A list of all master integrals from the four integral families in all 5! permutations *)
allIntegrals = Table[ { 
        Table[DP[p, i], {i, 108}], 
        Table[HB[p, i], {i, 73}], 
        Table[PB[p, i], {i, 61}],
        Table[P[p, i], {i, 11}]
    }, {p, 1, 120}] // Flatten // Sort;

Print["Evaluating ", Length[allIntegrals]," master integrals on the kinematical point ",X1point," ..."];

{timing, result} = EvaluateMI[allIntegrals, X1point] // AbsoluteTiming;

Print["took ",timing,"s"];

(*Export["targets/all_master_integrals_test_targets", (ToString[ NumberForm[N[result,33], 33, NumberFormat :> (If[#3 != "", Row[{#1, "*^", #3}], #1] &)]]),"Text"];*)

targets = Get["targets/all_master_integrals_test_targets"];

absError = result - targets // Flatten // Abs // Max;

Print["Maximal absolute error is ", absError];

Print[];

If[absError > 10^-6,
    Print["TEST FAILED!"];
    Exit[1];
    ,
    Print["Test passed."];
];

Exit[0];
