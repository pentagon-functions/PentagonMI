Needs["PentagonFunctionsM`","../PentagonFunctionsM.m"];

relErr[x_, t_/;(t==0)] := Abs[x - t];
relErr[x_, t_] := Abs[x - t]/Abs[t];
relErr[x_List, t_List] := Max[MapThread[relErr, {x,t}]];


(*
    We evaluate all pentagon functions on a kinematical point with 
    Delta=0 and s[i,j] != 0
*)

allfunctions = {
        Table[F[1, 1, i], {i, 10}], 
        Table[F[1, 2, i], {i, 10}], 
        Table[F[1, 3, i], {i, 5}], 
        Table[F[2, 1, i], {i, 15}], 
        Table[F[2, 2, i], {i, 9}],
        Table[F[3, i], {i, 111}], 
        Table[F[4, i], {i, 472}]
    } // Flatten;


Xb = {1, -(1/4), 1/4, 1/2, 1/8 (-3 - 2 Sqrt[2])};

result = PentagonFunctionsM`EvaluatePentagonFunctions[allfunctions, Xb, WorkingPrecision->30, "IntegrationPrecisionGoal" -> 7][[All,2]];

targets = Get["targets/functions_delta_singular"];

error = With[
    {
        tocheck = CoefficientList[#, Log[DeltaS]]& /@ result
    },
    MapIndexed[
        relErr[#1,PadRight[targets[[First[#2]]],Length[#1]]] &
        ,
        tocheck
    ]
] // Flatten // Max;

Print["Maximal error is ", error];

Print[];

testfailed = False;

If[error > 10^-6,
    Print["TEST FAILED!"];
    testfailed = True;
    ,
    Print["passed"];
];

Print[];

(*
    The evaluation in the limit Delta->0 should be consistent with the evaluation in the bulk.
    We evaluate the divergent weight 4 functions on a kinematical point XbD, that is slightly away from the Delta=0 surface, 
    treating it as generic kinematics, and compare to the previous result.
*)

XbD = {1, -(1/4), 1/4, 1/2, -(18463/25342)};

Print["Difference between the point with Delta=0 and the deformed point: "];
Xb-XbD//N//Print;

w4div = {425, 428, 430, 431, 432, 434, 435};

result2 = PentagonFunctionsM`EvaluatePentagonFunctions[F[4,#]& /@ w4div,XbD, "IntegrationPrecisionGoal" -> 5, "CheckZero"->True][[All,2]];

error = With[
    {
        ds = Echo[Sqrt[PentagonFunctionsM`Private`Delta/.Thread[Array[PentagonFunctionsM`v,5]->XbD]]//N, "Sqrt[Delta] = "],
        tocompare = result[[w4div-473]]
    },
     relErr[(tocompare/.DeltaS->ds), result2]
];

Print["Maximal relative difference is ",error];
If[Max[error] > 10^-4,
    Print["TEST FAILED!"];
    testfailed = True;
    ,
    Print["passed"];
];

Print[];


(*
    We evaluate the 2 weight 3 functions, that could diverge as Log[Sqrt[Delta]], also on a different point.
    On this point F[3,98] does diverge, and F[3,103] does not.
    On the previous point both of them are finite.
*)

Xb = {3, -1 + Sqrt[3]/2, 1, 1, -1};

result = PentagonFunctionsM`EvaluatePentagonFunctions[{F[3,98],F[3,103]}, Xb, "IntegrationPrecisionGoal" -> 7][[All,2]];

targets = 
    {
        {107.1792597669901998368`17.3 + 744.150640327195777`18.1*I, -473.7410112522892137040555679940`31.7},
        {153.0495885369210886551`17.6, 0}
    };

result = CoefficientList[#,{Log[DeltaS]},{2}]& /@ result;

error = relErr[result, targets];

Print["Maximal error is ", error];

Print[];

If[error > 10^-7,
    Print["TEST FAILED!"];
    testfailed = True;
    ,
    Print["passed"];
];

If[testfailed, Exit[1];, Exit[0]];
