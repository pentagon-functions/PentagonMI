Needs["PentagonFunctionsM`","../PentagonFunctionsM.m"];

If[FindFile["PentagonFunctions`"] === $Failed,
    Print["ERROR: PentagonFunctions++ library was not found!\nIt is required in this test. Exiting."];
    Exit[1];
];

Needs["PentagonFunctions`"];


X1point = {4, -(113/47), 281/149, 349/257, -(863/541)};

(* A list of all pentagon functions *)
allfunctions = {
        Table[F[1, 1, i], {i, 10}], 
        Table[F[1, 2, i], {i, 10}], 
        Table[F[1, 3, i], {i, 5}], 
        Table[F[2, 1, i], {i, 15}], 
        Table[F[2, 2, i], {i, 9}], 
        Table[F[3, i], {i, 111}], 
        Table[F[4, i], {i, 472}]
    } // Flatten;


Print["Evaluating with C++ library..."];

{evaluatorprocess, flist} = PentagonFunctions`StartEvaluatorProcess[allfunctions];
{timing1, result1} = PentagonFunctions`EvaluateFunctions[evaluatorprocess, X1point // N] // AbsoluteTiming;
KillProcess[evaluatorprocess];

Print["took ",timing1,"s\n"];


Print["Evaluating with Mathematica..."];

{timing2, result2} = PentagonFunctionsM`EvaluatePentagonFunctions[allfunctions, X1point // N] // AbsoluteTiming;

Print["took ",timing2,"s\n"];

absError = result1 - result2[[All,2]] // Flatten // Abs // Max;

Print["Maximal absolute difference is ", absError];

Print[];

If[absError > 10^-9,
    Print["TEST FAILED!"];
    Exit[1];
    ,
    Print["Test passed."];
];

Exit[0];
